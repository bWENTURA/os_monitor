#include "header.hpp"
#include "fifo_queue.h"

fifo_queue * get_new_queue(){
    fifo_queue * new_queue = (fifo_queue *)malloc(sizeof * new_queue); 
    new_queue->first_element = 0;
    new_queue->last_element = 0;
    new_queue->full = false;
    return new_queue;
}

void add_element(int new_value, fifo_queue * queue){
    if(queue->full){
        printf("Queue is full.\n");
    }
    else{
        queue->buffer[queue->last_element] = new_value;
        queue->last_element = (queue->last_element + 1) % 20;
        if(queue->last_element == queue->first_element)
            queue->full = true;
    }
}

void delete_element(fifo_queue * queue){
    if(queue->first_element == queue->last_element && !queue->full)
        printf("No element in list.\n");
    else{
        if(queue->first_element == queue->last_element)
            queue->full = false;
        queue->first_element = (queue->first_element + 1) % MAX;
    }
}

int check_first_out_value(fifo_queue * queue){
    if(queue->first_element == queue->last_element && !queue->full)
        return -1;
    else
        return queue->buffer[queue->first_element];
}

int count_odd(fifo_queue * queue){
    int iterator = queue->first_element;
    int count = 0;
    if(queue->first_element == queue->last_element && !queue->full)
        return 0;
    else{
        do{
            if(queue->buffer[iterator] % 2 != 0)
                ++count;
            iterator = (iterator + 1) % MAX;
        } while(iterator != queue->last_element);
        return count;
    }
}

int count_even(fifo_queue * queue){
    int iterator = queue->first_element;
    int count = 0;
    if(queue->first_element == queue->last_element && !queue->full)
        return 0;
    else{
        do{
            if(queue->buffer[iterator] % 2 == 0)
                ++count;
            iterator = (iterator + 1) % MAX;
        } while(iterator != queue->last_element);
        return count;
    }
}

void delete_queue(fifo_queue * queue){
    free(queue);
}

void show_queue(fifo_queue * queue){
    int iterator = queue->first_element;
    if(iterator == queue->last_element && !queue->full)
        printf("Queue is empty.\n");
    else{
        printf("[");
        do{
            printf("%d", queue->buffer[iterator]);
            iterator = (iterator + 1) % MAX;
            if(iterator == queue->last_element)
                printf("]\n");
            else
                printf(", ");
        }   while(iterator != queue->last_element);
    }
}

