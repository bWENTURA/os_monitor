#include "header.hpp"
#include "my_monitor.hpp"
#include "functions.h"

MyMonitor::MyMonitor() : Monitor()
{
    Condition * even_producer_waiting = new Condition();
    Condition * odd_producer_waiting = new Condition();
    Condition * even_consumer_waiting = new Condition();
    Condition * odd_consumer_waiting = new Condition();
    shared_queue = get_new_queue();
}

void MyMonitor::produce_even()
{
    enter();
    if (!even_can_produce(shared_queue))
        wait(even_consumer_condition);

    printf("In even producer operation - status = even = %d, odd = %d.\n",
           count_even(shared_queue), count_odd(shared_queue));

    add_element(generate_even(), shared_queue);
    show_queue(shared_queue);

    if (odd_can_consume(shared_queue))
        signal(odd_consumer_condition);
    else if (odd_can_produce(shared_queue))
        signal(odd_producer_condition);
    else if (even_can_consume(shared_queue))
        signal(even_consumer_condition);

    leave();

    sleep(1);
}

void MyMonitor::produce_odd()
{
    enter();
    if (!odd_can_produce(shared_queue))
        wait(odd_producer_condition);

    printf("In odd producer operation - status = even = %d, odd = %d.\n",
           count_even(shared_queue), count_odd(shared_queue));

    add_element(generate_odd(), shared_queue);
    show_queue(shared_queue);

    if (even_can_consume(shared_queue))
        signal(even_consumer_condition);
    else if (even_can_produce(shared_queue))
        signal(even_producer_condition);
    else if (odd_can_consume(shared_queue))
        signal(odd_consumer_condition);

    leave();

    sleep(1);
}

void MyMonitor::consume_even()
{
    enter();
    if (!even_can_consume(shared_queue))
        wait(even_consumer_condition);

    sleep(1);

    printf("In even consumer operation. - status = even = %d, odd = %d, to_delete = %d.\n",
           count_even(shared_queue), count_odd(shared_queue), check_first_out_value(shared_queue));

    delete_element(shared_queue);
    show_queue(shared_queue);

    if (odd_can_produce(shared_queue))
        signal(odd_producer_condition);
    else if (odd_can_consume(shared_queue))
        signal(odd_consumer_condition);
    else if (even_can_produce(shared_queue))
        signal(even_producer_condition);

    leave();
}

void MyMonitor::consume_odd()
{
    enter();

    if (!odd_can_consume(shared_queue))
        wait(odd_consumer_condition);

    sleep(1);

    printf("In odd consumer operation - status = even = %d, odd = %d, to_delete = %d.\n",
           count_even(shared_queue), count_odd(shared_queue), check_first_out_value(shared_queue));

    delete_element(shared_queue);
    show_queue(shared_queue);

    if (even_can_produce(shared_queue))
        signal(even_producer_condition);
    else if (odd_can_produce(shared_queue))
        signal(odd_producer_condition);
    else if (even_can_consume(shared_queue))
        signal(even_consumer_condition);
    
    leave();
}

MyMonitor::~MyMonitor()
{
    delete_queue(shared_queue);
}
