#include "header.hpp"
#include "fifo_queue.h"
#include "functions.h"

int generate_even(){
    int i = rand();
    while(i % 2)
        i = rand();
    return i % 100;
}

int generate_odd(){
    int i = rand();
    while(!(i % 2))
        i = rand();
    return i % 100;
}

bool even_can_produce(fifo_queue * queue){
    if(count_even(queue) < 10)
        return true;
    return false;
}

bool odd_can_produce(fifo_queue * queue){
    if(count_even(queue) > count_odd(queue))
        return true;
    return false;
}

bool even_can_consume(fifo_queue * queue){
    if((count_even(queue) + count_odd(queue) > 2) && !(check_first_out_value(queue) % 2))
        return true;
    return false;
}

bool odd_can_consume(fifo_queue * queue){
    if((count_even(queue) + count_odd(queue) > 6) && (check_first_out_value(queue) % 2))
        return true;
    return false;
}
