#ifndef MONITOR_H
#define MONITOR_H

#include <pthread.h>
#include "fifo_queue.h"
#include "monitor.h"

class MyMonitor: public Monitor
{
    fifo_queue *shared_queue;
    int even_producer_waiting, odd_producer_waiting, even_consumer_waiting, odd_consumer_waiting;
    Condition even_producer_condition, odd_producer_condition, even_consumer_condition, odd_consumer_condition;
    public:
        MyMonitor();
        void produce_even();
        void produce_odd();
        void consume_even();
        void consume_odd();
        ~MyMonitor();
};


#endif