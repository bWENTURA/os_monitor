#include "header.hpp"
#include "my_monitor.hpp"

void * produce_even_number(void * args);

void * produce_odd_number(void * args);

void * consume_even_number(void * args);

void * consume_odd_number(void * args);

int main(void){
    srand(time(NULL));
    MyMonitor * monitor = new MyMonitor();

    pthread_t even_producer_thread, odd_producer_thread, even_consumer_thread, odd_consumer_thread;

    pthread_create(&even_producer_thread, NULL, produce_even_number, (void *) monitor);
    pthread_create(&odd_producer_thread, NULL, produce_odd_number, (void *) monitor);
    pthread_create(&even_consumer_thread, NULL, consume_even_number, (void *) monitor);
    pthread_create(&odd_consumer_thread, NULL, consume_odd_number, (void *) monitor);

    pthread_join(even_producer_thread, NULL);
    pthread_join(odd_producer_thread, NULL);
    pthread_join(even_consumer_thread, NULL);
    pthread_join(odd_consumer_thread, NULL);
    
    free(monitor);

    return 0;
}

void * produce_even_number(void * args){
    MyMonitor * monitor = (MyMonitor *) args;
    while(true)
        monitor->produce_even();
    printf("Even producer end.\n");
}

void * produce_odd_number(void * args){
    MyMonitor * monitor = (MyMonitor *) args;
    while(true)
        monitor->produce_odd();
    printf("Odd producer end.\n");
}

void * consume_even_number(void * args){
    MyMonitor * monitor = (MyMonitor *) args;
    while(true)
        monitor->consume_even();
    printf("Even consumer end.\n");
}

void * consume_odd_number(void * args){
    MyMonitor * monitor = (MyMonitor *) args;
    while(true){
        monitor->consume_odd();
    }
    printf("Odd consumer end.\n");
}